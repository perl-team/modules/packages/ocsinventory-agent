ocsinventory-agent (2:2.10.0-5) UNRELEASED; urgency=medium

  [ Carles Pina i Estany ]
  * Added po-debconf Catalan translation

 -- Carles Pina i Estany <carles@pina.cat>  Fri, 31 Jan 2025 15:30:46 +0100

ocsinventory-agent (2:2.10.0-4) unstable; urgency=medium

  [ Remus-Gabriel Chelu ]
  * Update Romanian translation (Closes: #1038923)

  [ Martin Bagge / brother ]
  * Update Swedish translation (Closes: #1055272)

 -- Yadd <yadd@debian.org>  Fri, 03 Nov 2023 16:01:36 +0400

ocsinventory-agent (2:2.10.0-3) unstable; urgency=medium

  * Team upload.
  * Use /bin/bash instead of /bin/sh in cronjob as the script uses a
    bashism. (Closes: #988245) (LP: #2004007)
  * Declare compliance with Debian Policy 4.6.2.
  * Drop unneeded (alternative) (test) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Refresh propagate-ccflags.patch: update buildflags handling.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 Jan 2023 02:43:30 +0100

ocsinventory-agent (2:2.10.0-2) unstable; urgency=medium

  * Update Brazilian Portuguese Translation (Closes: #1025037)

 -- Yadd <yadd@debian.org>  Tue, 29 Nov 2022 06:43:44 +0100

ocsinventory-agent (2:2.10.0-1) unstable; urgency=medium

  [Cyrille Bollu]
  * Adds a random sleep in the cron.daily job

  [ gregor herrmann ]
  * Replace deprecated 'which' with 'command -v' in maintainer script(s).
  * Update Spanish debconf template.
    Thanks to Camaleón. (Closes: #1003642)

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on perl.
    + ocsinventory-agent: Drop versioned constraint on perl in Depends.
  * Update standards version to 4.6.0, no changes needed.

  [ Yadd ]
  * Update standards version to 4.6.1, no changes needed.
  * New upstream version 2.10.0
  * Refresh patches
  * Add systemd timer

 -- Yadd <yadd@debian.org>  Thu, 24 Nov 2022 15:55:32 +0100

ocsinventory-agent (2:2.8-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Add "Rules-Requires-Root: no"
  * New upstream version 2.8
  * Drop no-useless-concat patch, now included in upstream
  * Fix some spelling errors (reported)

 -- Xavier Guimard <yadd@debian.org>  Mon, 14 Sep 2020 13:11:49 +0200

ocsinventory-agent (2:2.4.2-3) unstable; urgency=medium

  * Update ru translation. Thanks to dogsleg! (Closes: #920736)
  * Danish translation. Thanks to Joe Dalton (Closes: #923076)
  * Declare compliance with policy 4.3.0
  * Remove dangling symlink (Closes: #567093)
  * Bump debhelper compatibility level to 12

 -- Xavier Guimard <yadd@debian.org>  Sun, 24 Feb 2019 11:06:50 +0100

ocsinventory-agent (2:2.4.2-2) unstable; urgency=medium

  * Email change: Xavier Guimard -> yadd@debian.org
  * Add no-useless-concat.patch (reported) (Closes: #913108)
  * Declare compliance with policy 4.2.1

 -- Xavier Guimard <yadd@debian.org>  Wed, 07 Nov 2018 06:30:43 +0100

ocsinventory-agent (2:2.4.2-1) unstable; urgency=medium

  * Import upstream version 2.4.2
  * Declare conformance with Policy 4.1.5
  * Add libio-socket-ssl-perl, liblwp-protocol-https-perl, libnet-cups-
    perl and libparse-edid-perl in recommended dependencies
  * Remove libcompress-zlib-perl from dependencies (virtual package included
    in Perl)
  * debian/copyright: add Adaptec.pm license and update years
  * Remove spelling-errors.patch, fix-powerpc-cpu-detection.patch and
    alias-net-devices-not-reported.patch now included in upstream
  * Update patches offset
  * Update po files
  * debian/rules: Don't install
    /usr/share/perl5/Ocsinventory/Agent/Backend/OS/HPUX/README

 -- Xavier Guimard <x.guimard@free.fr>  Wed, 01 Aug 2018 11:49:49 +0200

ocsinventory-agent (2:2.4.1-2) unstable; urgency=medium

  [ Xavier Guimard ]
  * Add libnet-snmp-perl and libnet-netmask-perl in recommended dependencies
  * Debconf templates review
    + German (Closes: #898035)
    + Dutch (Closes: #898864)
  * Add patch to detect alias network devices (Closes: #899385)

  [ gregor herrmann ]
  * debian/copyright: add information about files in debian/po which have
    an explicit third-party copyright statement.

 -- Xavier Guimard <x.guimard@free.fr>  Wed, 23 May 2018 17:41:41 +0200

ocsinventory-agent (2:2.4.1-1) unstable; urgency=medium

  * Taken under Pkg-Perl umbrella (Closes: #895426)
  * Modify debian/watch to point to GitHub
  * Imported upstream version 2.4.1 (Closes: #631359, #735680, #872820)
    (Closes: #745809, #536784, #644320, #734530, #864727, #567093)
  * Patches
    - Remove fix-no-software.patch, kvm-detection.diff, pod-syntax.patch,
      exclude_curdir_recursion.patch now included in upstream
    - Update exclude_curdir_recursion.patch
    - Add spelling errors patch
  * Dependencies:
    - Add libnmap-parser-perl in suggested dependencies (Closes: #782585)
    - Replace libnet-ssleay-perl by libcrypt-ssleay-perl
    - Add e2fsprogs in recommended dependencies (Closes: #887230)
    - Add "fdisk | util-linux (<< 2.29.2-3~)" in dependencies
      (Closes: #872213)
  * Update debian/copyright
  * Add myself to uploaders
  * Add upstream/metadata
  * declare conformance with Policy 4.1.4
  * Bump Debhelper compat level to 10
  * Update quilt version to 3.0
  * Use dh in debian/rules
  * Add debian/gbp.conf to ignore upstream .gitignore
  * Remove Ocsinventory::Agent::Common useless manpage
  * Add Testsuite: autopkgtest-pkg-perl
  * Add ipdiscover in package (Closes: #885789) and change architecture to
    "any"
  * Add propagate-cflags.patch for hardening
  * Configure local reports directory (Closes: #566914)
  * Update README.Debian for new report storage and remove old files
    mentioned (Closes: #607454) and add this in debian/NEWS
  * Update Debconf template: "server" is an URL (Closes: #779356)
  * Fix PowerPC CPU detection with a patch (Closes: #682961)
  * Translations:
    - update fr.po

 -- Xavier Guimard <x.guimard@free.fr>  Mon, 23 Apr 2018 06:40:00 +0200

ocsinventory-agent (2:2.0.5-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * postrm purge: Only run ucf if it is available.  (Closes: #687463)

 -- Andreas Beckmann <anbe@debian.org>  Sun, 22 Jan 2017 00:00:41 +0100

ocsinventory-agent (2:2.0.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "FTBFS with '.' removed from perl's @INC":
    call perl with "-I." in debian/rules.
    (Closes: #834353)

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Aug 2016 21:02:20 +0200

ocsinventory-agent (2:2.0.5-1) unstable; urgency=low

  * Imported Upstream version 2.0.5
  * Bump Standards Version to 3.9.3
  * Update build dependencies
  * Remove leftover files from old version
  * Ack NMU (thanks Christian)

 -- Pierre Chifflier <pollux@debian.org>  Thu, 12 Apr 2012 23:01:47 +0200

ocsinventory-agent (2:2.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Slovak (Slavko).  Closes: #622135
    - Polish (Michał Kułach).  Closes: #660533

 -- Christian Perrier <bubulle@debian.org>  Mon, 27 Feb 2012 07:27:29 +0100

ocsinventory-agent (2:2.0-1) unstable; urgency=low

  * Imported Upstream version 2.0 (Closes: #629047)
  * Removed patch no-ExtUtils-Installed.diff, applied upstream
  * Bump Standards Version to 3.9.2
  * Add build-arch and build-indep targets (fix lintian warnings)
  * Remove temporary file in postinst (Closes: #602407)
  * Add hdparm to recommended packages (Closes: #609879)

 -- Pierre Chifflier <pollux@debian.org>  Fri, 29 Jul 2011 23:51:45 +0200

ocsinventory-agent (2:1.1.1-2.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fix encoding of Danish debconf translation.

 -- Christian Perrier <bubulle@debian.org>  Tue, 11 Jan 2011 22:07:19 +0100

ocsinventory-agent (2:1.1.1-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Do not use ExtUtils::Installed to find module (Closes: #590879)
    ExtUtils::Installed looks all the @INC directory for .pack files,
    including '.'. When the agent was launched by a cron task or in
    daemon mode, the current directory is / and so the whole system was
    scanned, thank you Remi COLLET for pointing this issue.

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Fri, 24 Sep 2010 22:08:09 +0200

ocsinventory-agent (2:1.1.1-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Explicitly use 1.0 as source format.
  * Drop Gonéri Le Bouder from Uploaders on his request
  * Fix pending l10n issues. Debconf translations:
    - Japanese (Hideki Yamane (Debian-JP)).  Closes: #564298
    - Danish (Joe Hansen).  Closes: #581037
    - Vietnamese (Clytie Siddall).  Closes: #581540

 -- Christian Perrier <bubulle@debian.org>  Mon, 17 May 2010 07:14:39 +0200

ocsinventory-agent (2:1.1.1-2) unstable; urgency=low

  * New upstream release

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 03 Jan 2010 16:29:14 +0100

ocsinventory-agent (2:1.1.1-1) unstable; urgency=low

  * New upstream release
   - disable kvm-detection.diff and pod-syntax.diff, merged upstream
  * ja.po translation added, thanks Hideki Yamane (Closes: #554093)

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Mon, 21 Dec 2009 23:10:03 +0100

ocsinventory-agent (2:1.1-1) unstable; urgency=low

  * New upstream release
  * debian/ocsinventory-agent.templates, change _Choices to __Choices
  * Bump the epoch since for dpkg, 1.1beta1 > 1.1

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat, 14 Nov 2009 19:50:29 +0100

ocsinventory-agent (1:1.1beta1-1) experimental; urgency=low

  * New upstream beta release
  * Standard Version: 3.8.3
    - add a minimalist README.source
  * Don't check for /usr/bin/ucf presence to make lintian happy, BTW there
    is already a dependency on ucf

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Fri, 18 Sep 2009 17:45:14 +0200

ocsinventory-agent (1:1.0.1-4) unstable; urgency=low

  * Add ru translation (Closes: #541757)

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Mon, 17 Aug 2009 11:21:58 +0200

ocsinventory-agent (1:1.0.1-3) unstable; urgency=low

  * Add two new packages in suggests:
    - read-edid can be used to identify the screen
    - smartmontools is used to detect RAID system

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Tue, 28 Jul 2009 16:05:59 +0200

ocsinventory-agent (1:1.0.1-2) unstable; urgency=low

  * Fails to install/upgrade with mktemp 1.6-2, thanks Cyril Brulebois
    (Closes: #521213)
  * Bump Standard Version to 3.8.1
  * Drop the debian/: file
  * Add Swedish translation, thanks Martin Bagge (Closes: #506598)

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Wed, 25 Mar 2009 21:38:59 +0100

ocsinventory-agent (1:1.0.1-1) unstable; urgency=low

  * New upstream release
   - update debian/copyright
   - Suggests: nmap
   - drop the patches, merged upstream
   - calls ucf with --debconf-ok, when it should reuse the debconf instance
  * clean up debian/rules

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 22 Mar 2009 20:29:36 +0100

ocsinventory-agent (1:0.0.9.2repack1-5) unstable; urgency=low

  * New patch exclude_curdir_recursion:
    Do not recurse all directories from . (especially when . is /)
    Closes: #506416 (important)

 -- Pierre Chifflier <pollux@debian.org>  Fri, 21 Nov 2008 12:53:55 +0100

ocsinventory-agent (1:0.0.9.2repack1-4) unstable; urgency=low

  * fix: --nosoftware option has no effect and give a warning
   - add patch from upstream CVS, thanks Laurent Léonard (Closes: #497710)

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Fri, 05 Sep 2008 16:55:52 +0200

ocsinventory-agent (1:0.0.9.2repack1-3) unstable; urgency=low

  * Add Romanian debconf template (Closes: #488738)

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat, 09 Aug 2008 14:15:19 +0200

ocsinventory-agent (1:0.0.9.2repack1-2) unstable; urgency=low

  * Fix path in post[inst,rm} scripts
  * Bump standards version to 3.8.0
  * Update homepage

 -- Pierre Chifflier <pollux@debian.org>  Wed, 11 Jun 2008 12:02:26 +0200

ocsinventory-agent (1:0.0.9.2repack1-1) unstable; urgency=low

  * repack the archive to avoid the non-free IETF RFC/I-D,
    thanks Simon Josefsson (Closes: #478107)
  * remove the first entry of the changelog. It'd been created
    by dch by mistake
  * clean: remove quilt's .pc directory

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 27 Apr 2008 18:54:47 +0200

ocsinventory-agent (1:0.0.9.2-1) unstable; urgency=low

  [ Pierre Chifflier ]
  * Add Spanish debconf template (Closes: #476283)

  [ Gonéri Le Bouder ]
  * New upstream release (Closes: #476549)
  * Add myself as comaintainer
  * Fix the watchfile to use CPAN

 -- Pierre Chifflier <pollux@debian.org>  Sat, 19 Apr 2008 12:42:42 +0200

ocsinventory-agent (1:0.0.8-3) unstable; urgency=low

  * Add dependency on libproc-daemon-perl (Closes: #475410)
  * Call db_stop after ucf (Closes: #469513)
  * Add Brazilian Portuguese debconf templates (Closes: #471484)

 -- Pierre Chifflier <pollux@debian.org>  Sat, 12 Apr 2008 15:05:29 +0200

ocsinventory-agent (1:0.0.8-2) unstable; urgency=low

  * Call db_stop after ucf (Closes: #468101)

 -- Pierre Chifflier <pollux@debian.org>  Mon, 10 Mar 2008 10:19:14 +0100

ocsinventory-agent (1:0.0.8-1) unstable; urgency=medium

  * New upstream release, without RFC files (Closes: #463937)
  * urgency medium because bug is RC

 -- Pierre Chifflier <pollux@debian.org>  Tue, 05 Feb 2008 16:47:03 +0100

ocsinventory-agent (1:0.0.7-1) unstable; urgency=low

  * New upstream release (switch to ocsinventory-agent)
  * Add epoch to handle change in version
  * Set arch to all
  * Drop patches
  * Update my email address
  * Disable watch file temporarily
  * Ask for server only if using http method
  * Bum standards version (no changes)

 -- Pierre Chifflier <pollux@debian.org>  Fri, 25 Jan 2008 21:34:50 +0100

ocsinventory-agent (1.01-6) unstable; urgency=medium

  * Build only for architectures where dmidecode is available
  (Closes: #431300)
  * urgency=medium because of RC bug

 -- Pierre Chifflier <chifflier@inl.fr>  Mon, 02 Jul 2007 00:34:59 +0200

ocsinventory-agent (1.01-5) unstable; urgency=low

  * Debconf templates review (Closes: #422695)
    + Arabic (Closes: #423613)
    + Bulgarian (Closes: #423537)
    + Czech (Closes: #426053)
    + Dutch (Closes: #425712)
    + French (Closes: #426347)
    + Galician (Closes: #423546)
    + German (Closes: #425160)
    + Italian (Closes: #426254)
    + Portuguese (Closes: #422488)

 -- Pierre Chifflier <chifflier@inl.fr>  Tue, 29 May 2007 10:32:55 +0200

ocsinventory-agent (1.01-4) unstable; urgency=medium

  * Install file ocsinv.adm in correct path, and do not try to copy it
    (Closes: #422189)
  * Urgency = medium because of the above bug

 -- Pierre Chifflier <chifflier@inl.fr>  Fri,  4 May 2007 09:55:38 +0200

ocsinventory-agent (1.01-3) unstable; urgency=low

  * Fix logrotate file (Closes: #419346)
  * Fix ocsinv.adm path in create-ucf-config-files.pl to set tag correctly
    (Closes: #419355)

 -- Pierre Chifflier <chifflier@inl.fr>  Sun, 15 Apr 2007 12:00:01 +0200

ocsinventory-agent (1.01-2) unstable; urgency=low

  * Debconf translations:
    + add German (Closes: #418751)

 -- Pierre Chifflier <chifflier@inl.fr>  Sat, 14 Apr 2007 11:13:03 +0200

ocsinventory-agent (1.01-1) unstable; urgency=low

  * New upstream release
  * Update my email address

 -- Pierre Chifflier <chifflier@inl.fr>  Tue, 03 Apr 2007 10:39:15 +0200

ocsinventory-agent (1.0+debian-1) unstable; urgency=low

  * New upstream release
  * Repackage upstream tarball to remove CVS and tgz files

 -- Pierre Chifflier <chifflier@cpe.fr>  Sun, 28 Jan 2007 21:18:27 +0100

ocsinventory-agent (1.0~rc3-1) unstable; urgency=low

  * Initial release (Closes: #406964) based on the work of Vincent Danjean

 -- Pierre Chifflier <chifflier@cpe.fr>  Mon, 15 Jan 2007 12:28:59 +0100
